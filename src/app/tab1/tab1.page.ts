import { Component } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  food1: number;
  food2: number;
  food3: number;
  callories: number;

  constructor() {

    this.food1 = 0;
    this.food2 = 0;
    this.food3 = 0;
    this.callories = 0;
  }

  calculate() {
    let num = 0;
    num = this.food1 + this.food2 + this.food3;
    this.callories = num;
  }

}
