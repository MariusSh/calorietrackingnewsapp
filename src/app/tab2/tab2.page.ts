import { Component } from '@angular/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  calories: number;
  gender: string;

  height: number;
  weight: number;
  bmi: number;

  age: number;
  upperlimit: number;
  lowerlimit: number;

  constructor() {
    this.calories = 0;
    this.gender = "male";

    this.height = 0;
    this.weight = 0;
    this.bmi = 0;

    this.age = 0;
    this.upperlimit = 0;
    this.lowerlimit = 0;
  }

  calculate(): void{

    if (this.gender === "male" ) {
      
     this.calories = 66.47 + (13.7 * this.weight) + (5 * this.height) - (6.8 * this.age);

    } else {
      
      this.calories = 655.1 + (9.6 * this.weight) + (1.8 * this.height) - (4.7 * this.age);

    }
    this.calories = Math.round(this.calories * 100) / 100;
    this.bmi = this.weight / (Math.pow(this.height, 2));
    this.upperlimit = (220 - this.age) * 0.85;
    this.lowerlimit = (220 - this.age) * 0.65;
  }
}
