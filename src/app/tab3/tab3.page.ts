import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  items: any;
  searchQuery;


  constructor(
    private newsService: NewsService,
    private router: Router) {
  }

    ngOnInit(){
      
      this.newsService
      .getData('top-headlines?country=us&category=health') 
      .subscribe(data => {
        this.items = data;
      });
    }

    openNews(article) {
      this.newsService.currentNews = article;
      this.router.navigate(['/details']);
    }

   search(){
      this.newsService
      .getData('everything?q='+this.searchQuery)
      .subscribe(data => {
        this.items = data;
      });
    }
  

}
