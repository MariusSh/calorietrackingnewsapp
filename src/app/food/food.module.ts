import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-uploader',
  templateUrl: './food.page.html',
  styleUrls: ['./food.page.scss'],
})
export class FoodPage implements OnInit {

  constructor(
    private alertController: AlertController,
  ) {}

  ngOnInit() {
  }

  async editFood(food) {
    const alert = await this.alertController.create({
      header: 'Edit Food',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Example : Pizza Slice',
          value: food.name
        },
        {
          name: 'calories',
          type: 'number',
          placeholder: 'Example : 250',
          value: food.calories
        }
      ],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Save',
          handler: data => {
          }
        }
      ]
    });

    await alert.present();
  }

  async addFood() {
    const alert = await this.alertController.create({
      header: 'Add Food',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Example : Pizza Slice'
        },
        {
          name: 'calories',
          type: 'number',
          placeholder: 'Example : 250'
        }
      ],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Add',
          handler: data => {
          }
        }
      ]
    });

    await alert.present();
  }

  async deleteFood(food) {
    const alert = await this.alertController.create({
      header: 'Delete Food',
      message: 'Are you sure to delete this food menu?',
      buttons: [
        {
          text: 'Cancel'
        },
      ]
    });

    await alert.present();
  }
}
